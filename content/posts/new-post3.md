---
title: This is the title of a new post
author: "John Johnson"
date: "20. August 2020"
description: "Kijkend naar de langere termijn: bijna 40 jaar aan satellietgegevens van Groenland tonen aan dat de gletsjers op het eiland zo sterk zijn gekrompen dat zelfs als de klimaatopwarming vandaag zou stoppen"
image: flower3
---

### Aanhoudende onbalans Groenlandse ijskap:
Kijkend naar de langere termijn: bijna 40 jaar aan satellietgegevens van Groenland tonen aan dat de gletsjers op het eiland zo sterk zijn gekrompen dat zelfs als de klimaatopwarming vandaag zou stoppen, de ijskap nog geruime tijd zou blijven krimpen. Deze bevinding, die op 13 augustus in Nature Communications Earth and Environment is gepubliceerd, betekent dat de Groenlandse gletsjers nu in een staat van voortdurende onbalans zijn. De sneeuwval die de ijskap ieder jaar aanvult, weegt niet op tegen het ijs dat van de gletsjers de oceaan in stroomt.


### Meer informatie:

- ‘Return to rapid ice loss in Greenland and record loss in 2019 detected by the GRACE-FO satellites’, Nature Communications Earth & Environment, 20 augustus 2020, https://www.nature.com/articles/s43247-020-0010-1
- State of the Climate 2019 – Hoofdstuk 5 ‘The Arctic', speciaal online supplement bij het Bulletin of the American Meteorological Society, jaargang 101, nummer 8, augustus 2020
- Dynamic ice loss from the Greenland Ice Sheet driven by sustained glacier retreat’, King, M.D., Howat, I.M., Candela, S.G. et al., Nature Communications Earth & Environment, 13 augustus 2020, https://doi.org/10.1038/s43247-020-0001-2

## Advanced features:

- Easy customizable base **styles** via `theme` object (fonts, colors, sizes)
- **Components** lazy loading (social sharing, comments)
- **ESLint** (google config)
- **Prettier** code styling
- Custom webpack `CommonsChunkPlugin` settings
- Webpack `BundleAnalyzerPlugin`
- Contact **form validation** (react-material-ui-form-validator)
