import React, { useEffect, useState } from "react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Hero from "../components/Hero";
import Text from "../components/Text";
import Publications from "../components/Publications";
import MarkdownSidebar from "../components/MarkdownSidebar";
import Highlights from "../components/Highlights";
import FormattedData from "../components/FormattedData";
import { useIndexQuery } from "../hooks/useIndexQuery";
import Divider from "../components/Divider";
import Team from "../components/Team";
require(`gatsby-remark-mathjax-ssr/mathjax.css`);

const Index = () => {
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    setLoaded(true);
  }, []);

  const { filteredNav, articles, images } = FormattedData();
  const data = useIndexQuery();
  const { parts, components, partsArray } = data;

  return (
    <Layout data={filteredNav}>
      <SEO title="" />
      {loaded
        ? partsArray.map((node, i) => {
            switch (Object.keys(parts[node])[0]) {
              case "text":
                return <Text key={i} {...components[i]} />;
              case "divider":
                return <Divider key={i} {...components[i]} />;
              case "hero":
                return (
                  <Hero
                    key={i}
                    {...components[i]}
                    image={images.find(
                      (image) => image.parent.name === parts[node].hero.image
                    )}
                  />
                );
              case "team":
                const team = data.team.edges.find(
                  (el) => el.node.people !== null
                );
                images.forEach((image) => {
                  team.node.people.forEach((person) => {
                    if (image.parent.name === person.image)
                      person["fluidImage"] = image.fluid;
                  });
                });

                return (
                  <Team key={i} {...components[i]} team={team.node.people} />
                );
              case "publications":
                const yml = data.publications.edges.find(
                  (el) => el.node.publications !== null
                );
                return (
                  <Publications
                    key={i}
                    {...yml.node}
                    title={components[i].title}
                  />
                );
              case "highlights":
                images.forEach((image) => {
                  components[i].forEach((card) => {
                    if (image.parent.name === card.image)
                      card["fluidImage"] = image.fluid;
                  });
                });
                return <Highlights key={i} highlights={components[i]} />;
              case "markdown":
                const newsHtml = data.mdPosts.edges.find(
                  (element) =>
                    element.node.parent.name === components[i].filename
                );
                let sidebar = false;

                if (components[i].sidebar) {
                  if (components[i].sidebar !== "articles") {
                    sidebar = data.mdPosts.edges.find(
                      (element) =>
                        element.node.parent.name === components[i].sidebar
                    );
                  } else {
                    sidebar = "articles";
                  }
                }
                return (
                  <MarkdownSidebar
                    cards={articles}
                    markdown={newsHtml.node.html}
                    sidebar={sidebar}
                  />
                );
              default:
                return null;
            }
          })
        : ""}
    </Layout>
  );
};

export default Index;
