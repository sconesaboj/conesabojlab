import React from "react";
import Layout from "../components/layout";
import Link from "gatsby-link";
import FormattedData from "../components/FormattedData";

const NotFoundPage = () => {
  const { filteredNav } = FormattedData();

  return (
    <Layout data={filteredNav}>
      <div className="min-h-screen text-2xl text-white flex items-center justify-center flex-col">
        <div className="text-center transform -translate-y-20 text-black">
          <h1 className="mb-5 font-bold">Not Found.</h1>
          <p className="mb-5">You just hit a route that doesn&#39;t exist.</p>
          <Link to="/">Back to Home Page</Link>
        </div>
      </div>
    </Layout>
  );
};

export default NotFoundPage;
