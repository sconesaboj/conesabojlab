import React, { useState, useEffect, useContext } from "react";
import Link from "gatsby-link";
import BackgroundImage from "gatsby-background-image-es5";

const Hero = ({
  title,
  image,
  text,
  link1,
  link2,
  linkText1,
  linkText2,
  height,
}) => {
  const [heroImage, setHeroImage] = useState(null);
  const [heightConf, setHeightConf] = useState(null);

  useEffect(() => {
    let heightConfig;
    switch (height) {
      case "full":
        return (heightConfig = "50vw");
      case "tall":
        return (heightConfig = "40vw");
      case "medium":
        return (heightConfig = "30vw");
      case "short":
        return (heightConfig = "20vw");
    }
    setHeroImage(image);
    setHeightConf(heightConfig);
  }, []);

  return (
    <>
      {heroImage && (
        <BackgroundImage fluid={heroImage.fluid}>
          <div className="relative w-full">
            <div className="z-10 relative"></div>
            <div className="bg-black bg-opacity-50 relative z-20">
              <div
                className="text-black font-hairline flex flex-col items-center justify-center md:h-50vh h-screen "
                style={{ height: heightConf || "" }}
              >
                <div
                  className="text-center mt-0 px-5"
                  style={{ maxWidth: "1200px", color: "white" }}
                >
                  <h1
                    className="text-5xl font-thin shadow-style mb-2"
                    style={{ letterSpacing: "1px" }}
                  >
                    {title}
                  </h1>
                  <p className="text-xl max-w-xs mb-2 ">{text}</p>
                </div>
                {link1 && (
                  <div className="mt-6 flex">
                    <Link to={link1} style={{ boxShadow: "none" }}>
                      <button className="px-6 py-3 transition duration-300 w-40 bg-blue-400 bg-opacity-0 hover:bg-opacity-25 text-blue-400 border-2 rounded-sm border-blue-400 mx-3 font-bold transform">
                        {linkText1}
                      </button>
                    </Link>
                    {link2 && (
                      <Link to={link1} style={{ boxShadow: "none" }}>
                        <button className="px-6 py-3 transition duration-300 w-40 bg-blue-400 mx-3 font-bold rounded-sm hover:bg-blue-300 transform">
                          {linkText2}
                        </button>
                      </Link>
                    )}
                  </div>
                )}
              </div>
            </div>
          </div>
        </BackgroundImage>
      )}
    </>
  );
};

export default Hero;
