import React from "react";
import PostCard from "./PostCard";

const MarkdownSidebar = ({ cards, markdown, sidebar, articles }) => {
  return (
    <div className="xl:mx-25 lg:mx-20 sm:mx-12 mx-5">
      <div
        className={`${sidebar &&
          "md:grid"} md:pr-0 gap-x-160 lg:gap-x-15 max-w-1440 mx-auto lg:pb-20 sm:pb-12 pb-8`}
        style={{ gridTemplateColumns: "auto 440px" }}
      >
        <div className={`${sidebar ? "sm:mr-10" : ""}`}>
          <section
            className="markdown"
            dangerouslySetInnerHTML={{
              __html: markdown,
            }}
          />
        </div>
        {sidebar && (
          <div className="w-full mt-15 pl-10 border-l-2 border-gray">
            {sidebar === "articles" ? (
              <div className="flex flex-wrap">
                <div className="w-full">
                  <h3 className="text-left text-4xl mb-10">Latest Articles</h3>
                </div>
                {cards.map((card) => {
                  return (
                    <div className="mb-8 mr-10">
                      <PostCard
                        title={card.frontmatter.title}
                        description={card.frontmatter.description}
                        date={card.frontmatter.date}
                        image={card.fluidImage}
                        slug={card.parent.name}
                      />
                    </div>
                  );
                })}
              </div>
            ) : (
              <div>
                <section
                  className="markdown"
                  dangerouslySetInnerHTML={{
                    __html: sidebar.node.html,
                  }}
                />
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default MarkdownSidebar;
