import React from "react";
import Person from "./Person";
import noPhoto from "../images/noPhoto.jpg";

const Team = ({ title, team, sorting }) => {
  if (sorting) {
    const sortingArr = sorting.split(", ");
    team.sort((a, b) =>
      sortingArr.indexOf(a.title) > sortingArr.indexOf(b.title) ? 1 : -1
    );
  }
  return (
    <>
      <div className="my-12 mx-auto max-w-md xl:mx-25 lg:mx-20 sm:mx-12 mx-5">
        <h1 className="ml-4 text-4xl text-center mb-10 text-blue-500">
          {title}
        </h1>
        <div
          className="grid gap-x-4 justify-center"
          style={{ gridTemplateColumns: "repeat(auto-fill, 330px)" }}
        >
          {team.map((person) => (
            <Person
              name={person.name}
              title={person.title}
              room={person.room}
              tel={person.tel}
              email={person.email}
              fluidImage={person.fluidImage || noPhoto}
            />
          ))}
        </div>
      </div>
    </>
  );
};

export default Team;
