import React from "react";
import Image from "gatsby-image";

const PublicationCard = ({ title, tagline, authors, image, link, url }) => {
  return (
    <div
      className="m-3 cursor-pointer max-w-300"
      style={{ border: "2px solid #e6e6e6" }}
    >
      <a href={url}>
        <div className="flex flex-col h-full justify-between">
          <div>
            <div className="w-full h-40 overflow-hidden mb-0">
              <Image
                fluid={image}
                className="h-40 object-cover"
                style={{ filter: "grayscale(0.7)" }}
              />
            </div>
            <div className="px-4 font-hairline  mt-4">
              <h3 className="text-lg text-green mb-4 font-bold leading-tight">
                {title}
              </h3>
              <p className="text-sm text-grey pb-3" style={{ color: "#777" }}>
                {tagline}
              </p>
            </div>
          </div>
          <p
            className="text-sm pt-4 pb-4 px-4 text-grey font-hairline italic"
            style={{ color: "#777", borderTop: "1px solid #e6e6e6" }}
          >
            {authors}
          </p>
          <a
            href={url}
            className="text-blue-400 text-sm pb-4 px-4"
            style={{ textDecoration: "none", borderBottom: "none" }}
          >
            {link}
          </a>
        </div>
        {/* </Link> */}
      </a>
    </div>
  );
};

export default PublicationCard;
