import React from "react";
import Link from "gatsby-link";
import Image from "gatsby-image";

const Person = ({ name, title, room, fluidImage, email, tel }) => {
  let nameSlug = name.toLowerCase();
  nameSlug = nameSlug.split(" ").join("-");
  return (
    <div
      className="mx-2 my-4 cursor-pointer transform transition duration-300"
      style={{ width: "300px", maxWidth: "300px" }}
    >
      <Link to={`/people/${nameSlug}`}>
        <div className="flex flex-col justify-between">
          <div
            className="flex h-20 overflow-hidden"
            style={{ height: "115px" }}
          >
            <div style={{ height: "115px", width: "115px" }}>
              <Image fluid={fluidImage} />
            </div>
            {/* <img
              src={image}
              style={{ height: "115px", width: "115px" }}
              className="object-cover"
            /> */}
            <div className="px-2 font-hairline py-1">
              <h5
                className="text-lg mb-1 font-bold"
                style={{ fontSize: "16px" }}
              >
                {name}
              </h5>
              <p className="text-sm">{title}</p>
              <p className="text-sm">{room}</p>
              <p className="text-sm">{tel}</p>
              <p className="text-sm">{email}</p>
            </div>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default Person;
