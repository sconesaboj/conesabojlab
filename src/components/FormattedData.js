import { useIndexQuery } from "../hooks/useIndexQuery";

const FormattedData = () => {
  const data = useIndexQuery();
  const { articles, navElements } = data;

  const images = data.images.edges.map((edge) => edge.node);

  images.forEach((image) => {
    articles.forEach((article) => {
      if (image.parent.name === article.frontmatter.image)
        article["fluidImage"] = image.fluid;
    });
  });

  const filteredNav = {};

  navElements.node.header.forEach((el) => {
    if (el.identity) {
      filteredNav.identity = el.identity[0];
      if (!filteredNav.identity.text) filteredNav.identity.text = false;
      if (filteredNav.identity.image) {
        images.forEach((image) => {
          if (image.parent.name === filteredNav.identity.image)
            filteredNav.identity["fluidImage"] = image.fluid;
        });
      } else {
        filteredNav.identity["fluidImage"] = false;
      }
    }
    if (el.navigation) {
      filteredNav.navigation = el.navigation;
      filteredNav.navigation.map((el) => {
        if (el.image)
          el.fluidImage = images.find(
            (image) => image.parent.name === el.image
          );
        if (el.dropdown) {
          el.dropdown.map((dropdownEl) => {
            if (dropdownEl.image)
              dropdownEl.fluidImage = images.find(
                (image) => image.parent.name === dropdownEl.image
              );
            return dropdownEl;
          });
        }
        return el;
      });
    }
  });

  return { filteredNav, articles, images };
};

export default FormattedData;
