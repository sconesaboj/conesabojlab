import React from "react";
import { graphql, useStaticQuery } from "gatsby";

const FooterSimple = () => {
  const data = useStaticQuery(graphql`
    query FooterQuery {
      contentYaml {
        footer
      }
    }
  `);
  const text = data.contentYaml.footer;
  return (
    <>
      <div className="p-2 bg-black ">
        <div className="flex justify-center w-full text-white py-4">{text}</div>
      </div>
    </>
  );
};

export default FooterSimple;
