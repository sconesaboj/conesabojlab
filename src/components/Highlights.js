import React from "react";
import PublicationCard from "./PublicationCard";

const Highlights = ( highlights, id, title ) => {
  return (
    <>
      <div className="mb-20 px-7 max-w-md mx-auto" id={id || "highlights"}>
        <h2 className="ml-4 text-4xl text-center my-12 text-blue-500">
          {title || "Highlights"}
        </h2>
        <div className="flex justify-center flex-wrap">
          {highlights.highlights.map((el) => {
            return <PublicationCard {...el} image={el.fluidImage} />;
          })}
        </div>
      </div>
    </>
  );
};

export default Highlights;
