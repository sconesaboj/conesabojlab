import React from "react";

const Text = ({ title, text, invert }) => {
  return (
    <div
      className={`bg-${invert ? "black" : "white"} py-10 text-${
        invert ? "white" : "black"
      }`}
    >
      <div className="mx-auto max-w-sm">
        <h2 className="text-center text-4xl">{title}</h2>
        <div className="w-full px-5 sm:px-10">
          <p className="text-center mt-8 lg:w-1/2 md:5/6 mx-auto font-thin">
            {text}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Text;
