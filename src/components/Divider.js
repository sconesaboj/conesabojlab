import React from "react";
import PropTypes from "prop-types";

const Divider = ({ spacing, color, visible }) => {
  return (
    <>
      <div
        style={{
          paddingTop: `${spacing > 0 ? spacing : ""}rem`,
          paddingBottom: `${spacing > 0 ? spacing : ""}rem`,
          marginTop: `${spacing < 0 ? spacing : ""}rem`,
          marginBottom: `${spacing < 0 ? spacing : ""}rem`,
          width: "100%",
        }}
      >
        <div
          style={{
            width: `25%`,
            borderBottom: `1px solid ${color}`,
            marginLeft: "auto",
            marginRight: "auto",
          }}
        />
      </div>
    </>
  );
};

Divider.defaultProps = {
  color: "#000",
  spacing: 4,
  visible: false,
};

export default Divider;
