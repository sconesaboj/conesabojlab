import React from "react";

const CollapsibleCard = ({
  title,
  authors,
  link,
  linkText,
  published,
  pdf,
  abstract,
}) => {
  return (
    <>
      <button type="button" className="collapsible my-1">
        <span className="font-bold">{title}</span>
        <span className="">
          <br />
          {authors}
        </span>
        <span className="">
          <br />
          <a href={link} target="_blank">
            {linkText}
          </a>
        </span>
        <span>
          {" "}
          [<a href={pdf}>pdf</a>],
        </span>
        <span> ({published})</span>
      </button>
      <div className="content -mt-4 mb-3">
        <p className="py-5 px-1 italic">{abstract}</p>
      </div>
    </>
  );
};

export default CollapsibleCard;
