import React from "react";
import Link from "gatsby-link";
import Image from "gatsby-image";

const PostCard = ({ title, description, date, image, slug }) => {
  console.log(slug);
  return (
    <Link to={`/posts/${slug}`} style={{ boxShadow: "none" }}>
      <div
        className="cursor-pointer max-w-300 postcard"
        style={{ border: "1px solid #e6e6e6" }}
      >
        <a href=" ">
          <div className="flex flex-col h-full justify-between">
            <div>
              <div className="w-full h-40 mb-0">
                <Image
                  fluid={image}
                  width="100%"
                  className="h-40 object-cover"
                  style={{ filter: "grayscale(0.7)" }}
                />
              </div>
              <div className="px-4 font-hairline mt-2">
                <h3 className="text-lg text-green mb-0 font-bold leading-tight">
                  {title}
                </h3>
                <small className="opacity-50 block mb-3">{date}</small>
                <p className="text-sm text-grey pb-3" style={{ color: "#777" }}>
                  {description}
                </p>
              </div>
            </div>
          </div>
        </a>
      </div>
    </Link>
  );
};

export default PostCard;
