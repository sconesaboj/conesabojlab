import React from "react";
import PostCard from "./PostCard";

const NewsContainer = (cards) => {
  return (
    <div className="mx-auto pt-12 pb-20 max-w-md px-20">
      <h1 className="text-center text-4xl mb-10 text-blue-500">
        Latest Articles
      </h1>
      <div className="flex flex-wrap justify-between">
        {cards.cards.map((card) => {
          return (
            <PostCard
              title={card.title}
              description={card.description}
              date={card.date}
              image={Image1}
            />
          );
        })}
      </div>
    </div>
  );
};

export default NewsContainer;
