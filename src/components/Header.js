import React, { useState, useEffect } from "react";
import Link from "gatsby-link";
import Headroom from "react-headroom";
import Image from "gatsby-image";
import { FaAngleDown } from "react-icons/fa";
import "../styles.css";
import * as Icon from "../svg";

const Header = ({ data }) => {
  const { identity, navigation } = data;

  return (
    <Headroom className="z-50 relative">
      <DesktopNav identity={identity} navigation={navigation} />
      <MobileNav identity={identity} navigation={navigation} />
    </Headroom>
  );
};

export default Header;

const DesktopNav = ({ identity, navigation }) => {
  const [logo, setLogo] = useState(null);

  useEffect(() => {
    if (identity.fluidImage) setLogo(identity.fluidImage);
  }, []);

  return (
    <div className="bg-black flex items-center lg:h-70px top-0 left-0 w-full 1200:block hidden">
      <div
        className="flex items-center w-full justify-between flex-col md:flex-row mx-auto sm:px-20"
        style={{ maxWidth: "1440px" }}
      >
        <div
          className="flex text-white mt-1 bg-black text-sm justify-between w-full"
          style={{ letterSpacing: "1px" }}
        >
          <div
            className="flex items-center font-bold"
            style={{ transform: "translateY(-2px)", fontSize: "16px" }}
          >
            {logo && (
              <div className="mr-3">
                <Link to="/">
                  <Image
                    fluid={logo}
                    alt="logo"
                    style={{
                      height: "45px",
                      width: "150px",
                      margin: 0,
                    }}
                    className="py-2"
                  />
                </Link>
              </div>
            )}
            <Link to="/" className="mt-2px p-0 outline-none shadow-none">
              {identity.text !== false && identity.text}
            </Link>
          </div>
          <div className="flex">
            <div className="flex items-center uppercase">
              {navigation &&
                navigation.map((el) => {
                  return (
                    <>
                      {/* DROPDOWN MENU ITEM */}
                      {el.dropdown ? (
                        <div className="relative group cursor-pointer">
                          <div className="droplist px-4 font-hairline py-2 no-underline duration-300 transition-all hover:text-blue-400">
                            <Link
                              style={{ textDecoration: "none" }}
                              to={`/${el.link}`}
                              activeClassName="text-blue-400"
                            >
                              <div className="flex items-center">
                                {el.fluidImage ? (
                                  <div className="w-32 h-10 headerimage">
                                    <Image
                                      fluid={el.fluidImage.fluid}
                                      alt={el.text}
                                      className=""
                                    />
                                  </div>
                                ) : (
                                  <p>{el.text}</p>
                                )}
                                <i className="pl-1 flex justify-center items-center">
                                  <FaAngleDown />
                                </i>
                                <i className="pl-1 flex justify-center items-center">
                                  {/* <FaAngleDown /> */}
                                </i>
                              </div>
                            </Link>
                          </div>
                          <div
                            className={`transform ${
                              el.fluidImage
                                ? "-translate-y-3"
                                : "-translate-y-2px"
                            }   absolute z-10 hidden group-hover:block`}
                          >
                            <div
                              className="transform -translate-y-2 translate-x-2 w-40 bg-white text-black shadow-lg"
                              style={{
                                transform: "translateY(12px) translateX(8px)",
                              }}
                            >
                              <div className="text-white transform -mt-px">
                                {el.dropdown.map((el) => (
                                  <Link to={`/${el.link}`}>
                                    {el.fluidImage ? (
                                      <div className="bg-black pb-2 ">
                                        <Image
                                          fluid={el.fluidImage.fluid}
                                          alt={el.text}
                                          className="h-8 w-32"
                                          style={{ marginBottom: 0 }}
                                        />
                                      </div>
                                    ) : (
                                      <div className="py-2 px-3 border border-black cursor-pointer bg-black transition duration-300 hover:text-blue-400">
                                        {el.text}
                                      </div>
                                    )}
                                  </Link>
                                ))}
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : (
                        // SIMPLE MENU ITEM
                        <div className="">
                          <div className="droplist relative px-4 font-hairline py-2 no-underline duration-300 transition-all hover:text-blue-400">
                            <Link
                              style={{ textDecoration: "none" }}
                              to={`/${el.link}`}
                              activeClassName="text-blue-400"
                            >
                              <div className="flex">
                                {el.fluidImage ? (
                                  <div className="w-32 h-10">
                                    <Image
                                      fluid={el.fluidImage.fluid}
                                      alt={el.text}
                                    />
                                  </div>
                                ) : (
                                  <p>{el.text}</p>
                                )}
                              </div>
                            </Link>
                          </div>
                        </div>
                      )}
                    </>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const MobileNav = ({ identity, navigation }) => {
  const [open, setOpen] = useState(false);
  const handleClick = () => {
    setOpen(!open);
  };
  const itemClick = (e) => {
    if (
      e.target.parentElement.parentElement.parentElement.lastElementChild.style
        .display !== "block"
    ) {
      e.target.parentElement.parentElement.parentElement.lastElementChild.style.display =
        "block";
      e.target.lastElementChild.style.transition = "0.3s ease all";
      e.target.lastElementChild.style.transform =
        "rotate(90deg) translateX(-2px) translateY(-23px)";
    } else {
      e.target.parentElement.parentElement.parentElement.lastElementChild.style.display =
        "";
      e.target.lastElementChild.style.transform = "";
    }
  };
  return (
    <>
      <div
        className={`bg-black block 1200:hidden py-5 px-6 xs:px-10 h-15 w-full flex justify-between z-50 relative`}
      >
        <div className="flex items-center">
          <div className="mr-3">
            <Link to="/">
              <Image
                fluid={identity.fluidImage}
                alt="logo"
                style={{
                  height: "45px",
                  width: "200px",
                  margin: 0,
                }}
                className="py-1 mr-3"
              />
            </Link>
          </div>
          <Link
            to="/"
            className="mt-2px p-0 outline-none shadow-none text-white"
          >
            {identity.text !== false && identity.text}
          </Link>
        </div>
        <div
          className="cursor-pointer relative"
          style={{ zIndex: "999" }}
          onClick={handleClick}
        >
          <div className="transition duration-300">
            <Icon.WhiteNav />
          </div>
        </div>
      </div>
      <header
        className={`text-black fixed overflow-y-scroll z-40 h-screen w-screen 1200:hidden bg-white left-0 top-0 transition duration-300 ${
          open ? "opacity-100 relative" : "opacity-0 pointer-events-none relative hidden"
        }`}
        style={{ zIndex: 70 }}
      >
        <ul className="list-style-none p-0 xs:px-10 px-5 pt-10">
          {navigation &&
            navigation.map((el) => {
              return (
                <li className="text-xl mb-10 w-full">
                  {el.dropdown ? (
                    <div className="cursor-pointer">
                      <div className="w-full flex justify-between">
                        <div className="">
                          {" "}
                          {el.fluidImage ? (
                            <div className="w-32 h-10">
                              <Image
                                fluid={el.fluidImage.fluid}
                                alt={el.text}
                              />
                            </div>
                          ) : el.link ? (
                            <Link style={{ boxShadow: "none" }} to={`/${el.link}`}>
                              {el.text}
                            </Link>
                          ) : (
                            <Icon.ArrowRight />
                          )}
                        </div>
                        <div
                          onClick={itemClick}
                          className="transform translate-y-1 translate-x-2"
                        >
                          <Icon.ArrowRight />
                        </div>
                      </div>
                      <ul className="text-base list-style-none p-0 mt-6 ml-5 hidden">
                        {el.dropdown.map((element) => {
                          return (
                            <li className="mb-5">
                              <Link
                                to={`/${element.link}`}
                                onClick={handleClick}
                                style={{ boxShadow: "none" }}
                              >
                                {element.fluidImage ? (
                                  <div className="w-32 h-10">
                                    <Image
                                      fluid={element.fluidImage.fluid}
                                      alt={element.text}
                                    />
                                  </div>
                                ) : (
                                  element.text
                                )}
                              </Link>{" "}
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  ) : (
                    <Link
                      to={`/${el.link}`}
                      className="w-full"
                      style={{
                        width: "100%",
                        boxShadow: "none",
                      }}
                    >
                      {el.fluidImage ? (
                        <div className="w-32 h-10">
                          <Image fluid={el.fluidImage.fluid} alt={el.text} />
                        </div>
                      ) : (
                        el.text
                      )}
                    </Link>
                  )}
                </li>
              );
            })}
        </ul>
      </header>
    </>
  );
};
