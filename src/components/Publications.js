import React, { useEffect, useState } from "react";
import "../collapsible.css";
import CollapsibleCard from "./CollapsibleCard";
import { document } from "browser-monads";

const Publications = ({ title, publications }) => {
  const list = publications.map((el) => el.publication);
  useEffect(() => {
    let coll = document.getElementsByClassName("collapsible");
    let i;
    const onLoad = () => {
      for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
          this.classList.toggle("active");
          let content = this.nextElementSibling;
          content.style.display === "block"
            ? (content.style.display = "none")
            : (content.style.display = "block");
        });
      }
    };
    onLoad();
  });
  return (
    <>
      <div className="-mt-8 absolute" id="list" />
      <h3 className="text-3xl text-center mb-10 " id="lit">
        {title}
      </h3>
      <div className="m-4 cursor-pointer mx-auto max-w-md sm:px-12 lg:px-32">
        {list.map((publication) => {
          return <CollapsibleCard {...publication} key={publication.id} />;
        })}
      </div>
    </>
  );
};

export default Publications;
