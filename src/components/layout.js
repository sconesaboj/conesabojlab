import React from "react";
import Header from "./Header";
import FooterSimple from "./FooterSimple";
import "../styles.css";

const Layout = ({ location, title, children, data }) => {
  const rootPath = `${__PATH_PREFIX__}/`;
  return (
    <>
      <Header data={data} />
      <main>{children}</main>
      <FooterSimple />
    </>
  );
};

export default Layout;
