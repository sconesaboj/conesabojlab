import React from "react";

const Markdown = ({ md }) => (
  <div className="w-full">
    <div className="max-w-1200 mx-auto">
      <section
        className="markdown px-20"
        dangerouslySetInnerHTML={{
          __html: md.node.html,
        }}
      />
    </div>
  </div>
);

export default Markdown;
