import React, { useEffect, useState } from "react";
import { graphql } from "gatsby";
import FormattedData from "../components/FormattedData";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Hero from "../components/Hero";
import GalleryContainer from "../components/GalleryContainer";
import Text from "../components/Text";
import Publications from "../components/Publications";
import MarkdownSidebar from "../components/MarkdownSidebar";
import Highlights from "../components/Highlights";
import Divider from "../components/Divider";
import Team from "../components/Team";
require(`gatsby-remark-mathjax-ssr/mathjax.css`);

// This page template is used by gatsby-node.js to generate pages dynamically
// based on yaml files located in ~/content/pages
const GeneratePage = ({ data }) => {
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    setLoaded(true);
  }, []);

  const { filteredNav, articles, images } = FormattedData();
  const { parts } = data.pagesYaml;

  parts.forEach((part) => {
    function clean(obj) {
      for (let propName in obj) {
        if (obj[propName] === null || obj[propName] === undefined) {
          delete obj[propName];
        }
      }
    }
    clean(part);
  });

  const partsArray = Object.keys(parts);
  const components = parts.map((part) => {
    return part[Object.keys(part)[0]];
  });

  return (
    <>
      <Layout data={filteredNav}>
        <SEO title="" />
        {loaded
          ? partsArray.map((node, i) => {
              switch (Object.keys(parts[node])[0]) {
                case "text":
                  return <Text key={i} {...components[i]} />;
                case "divider":
                  return <Divider key={i} {...components[i]} />;
                case "hero":
                  components[i].image = images.find(
                    (image) => image.parent.name === parts[node].hero.image
                  );
                  return <Hero key={i} {...components[i]} />;
                case "team":
                  const team = data.team.edges.find(
                    (el) => el.node.people !== null
                  );
                  images.forEach((image) => {
                    team.node.people.forEach((person) => {
                      if (image.parent.name === person.image)
                        person["fluidImage"] = image.fluid;
                    });
                  });
                  return (
                    <Team key={i} {...components[i]} team={team.node.people} />
                  );
                case "publications":
                  const yml = data.publications.edges.find(
                    (el) => el.node.publications !== null
                  );
                  return (
                    <Publications
                      key={i}
                      {...yml.node}
                      title={components[i].title}
                    />
                  );
                case "gallery":
                  console.log(images);
                  let galleryImages = images
                    .filter((image) =>
                      image.parent.absolutePath.includes(
                        `/${components[i].foldername}/`
                      )
                    )
                    .map((el) => el.parent.absolutePath);
                  return <GalleryContainer key={i} />;
                case "highlights":
                  images.forEach((image) => {
                    components[i].forEach((card) => {
                      if (image.parent.name === card.image)
                        card["fluidImage"] = image.fluid;
                    });
                  });
                  return <Highlights key={i} highlights={components[i]} />;
                case "markdown":
                  const newsHtml = data.mdPosts.edges.find(
                    (element) =>
                      element.node.parent.name === components[i].filename
                  );
                  let sidebar = false;

                  if (components[i].sidebar) {
                    if (components[i].sidebar !== "articles") {
                      sidebar = data.mdPosts.edges.find(
                        (element) =>
                          element.node.parent.name === components[i].sidebar
                      );
                    } else {
                      sidebar = "articles";
                    }
                  }
                  return (
                    <MarkdownSidebar
                      cards={articles}
                      markdown={newsHtml.node.html}
                      sidebar={sidebar}
                    />
                  );
                default:
                  return null;
              }
            })
          : ""}
      </Layout>
    </>
  );
};

export default GeneratePage;

export const query = graphql`
  query($slug: String!) {
    pagesYaml(slug: { eq: $slug }) {
      parts {
        team {
          title
          sorting
        }
        hero {
          image
          title
          text
          linkText1
          link1
          linkText2
          link2
        }
        text {
          text
          title
          invert
        }
        highlights {
          tagline
          title
          authors
          link
          url
          image
        }
        markdown {
          filename
          sidebar
        }
        divider {
          spacing
          color
          visible
        }
        publications {
          sorting
          title
        }
        gallery {
          foldername
        }
      }
      slug
    }
    publications: allContentTypesYaml {
      edges {
        node {
          publications {
            publication {
              authors
              abstract
              link
              linkText
              pdf
              published
              title
            }
          }
        }
      }
    }
    team: allContentTypesYaml {
      edges {
        node {
          people {
            email
            name
            room
            tel
            title
            image
          }
        }
      }
    }
    images: allImageSharp {
      edges {
        node {
          parent {
            ... on File {
              name
              absolutePath
            }
          }
          fluid {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
    mdPosts: allMarkdownRemark {
      edges {
        node {
          html
          parent {
            ... on File {
              name
            }
          }
        }
      }
    }
    mdArticles: allMarkdownRemark {
      edges {
        node {
          html
          frontmatter {
            author
            description
            image
            title
            date
          }
          parent {
            ... on File {
              id
              name
              absolutePath
            }
          }
        }
      }
    }
    navigation: allContentTypesYaml {
      edges {
        node {
          header {
            identity {
              text
              image
              link
            }
            navigation {
              dropdown {
                link
                text
                image
              }
              link
              text
              image
            }
          }
        }
      }
    }
  }
`;
