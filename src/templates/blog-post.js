import React from "react";
import { Link, graphql } from "gatsby";
import FormattedData from "../components/FormattedData";
import SEO from "../components/seo";
import Layout from "../components/layout";
import BackgroundImage from "gatsby-background-image-es5";

const BlogPostTemplate = ({ data, pageContext }) => {
  const { filteredNav, images } = FormattedData();
  const post = data.markdownRemark;

  console.log(data);
  const fluidImage = images.find(
    (el) => el.parent.name === data.markdownRemark.frontmatter.image
  );

  console.log(fluidImage);

  return (
    <>
      <Layout data={filteredNav}>
        <SEO
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
        />
        <article>
          <header>
            {fluidImage && (
              <BackgroundImage fluid={fluidImage.fluid}>
                <div className="text-center h-screen text-white sm:h-50vh bg-black bg-opacity-25 sm:px-20 px-6">
                  <div className="flex flex-col items-center justify-center h-full max-w-1440 mx-auto">
                    <h1 className="text-5xl font-black mb-1">
                      {post.frontmatter.title}
                    </h1>
                    <p className="text-sm leading-loose mb-1 ">
                      {post.frontmatter.description}
                    </p>
                    <p className="text-sm leading-loose italic">
                      {post.frontmatter.date}
                    </p>
                  </div>
                </div>
              </BackgroundImage>
            )}
          </header>
          <div className="xl:mx-25 lg:mx-20 sm:mx-12 mx-5">
            <div className="sm:pb-12 pb-5 max-w-1440 mx-auto">
              <section
                className="markdown"
                dangerouslySetInnerHTML={{ __html: post.html }}
              />
            </div>
          </div>
        </article>
      </Layout>
    </>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    images: allImageSharp {
      edges {
        node {
          parent {
            ... on File {
              name
            }
          }
          fluid {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
    navigation: allContentTypesYaml {
      edges {
        node {
          header {
            identity {
              text
              image
              link
            }
            navigation {
              dropdown {
                link
                text
              }
              link
              text
            }
          }
        }
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        title
        date
        description
        author
        image
      }
    }
    navigation: allContentTypesYaml {
      edges {
        node {
          header {
            identity {
              text
              image
              link
            }
            navigation {
              dropdown {
                link
                text
              }
              link
              text
            }
          }
        }
      }
    }
  }
`;
