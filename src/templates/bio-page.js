import React, { useState, useEffect } from "react";
import FormattedData from "../components/FormattedData";
import Image from "gatsby-image";
import Layout from "../components/layout";
import SEO from "../components/seo";
import peopleYaml from "../../content/content-types/people.yaml";

const Bio = ({ data }) => {
  const { name } = data.markdownRemark.frontmatter;
  const { filteredNav, images } = FormattedData();

  const [details, setDetails] = useState(null);
  const [scholarProfile, setScholarProfile] = useState(null);
  const [profileImage, setProfileImage] = useState(null);
  const [interests, setInterests] = useState(null);

  useEffect(() => {
    const details = peopleYaml.people.find((el) => el.name === name);
    const detailsArr = Object.entries(details).map((e) => ({
      [e[0]]: e[1],
    }));
    const profile = details["scholar profile"] || null;

    const filteredDetailsArr = detailsArr.filter(
      (el) =>
        Object.keys(el)[0] !== "scholar profile" &&
        Object.keys(el)[0] !== "interests" &&
        Object.keys(el)[0] !== "image"
    );

    const img = images.find((el) => el.parent.name === details.image);
    setInterests(data.markdownRemark.frontmatter.interests);
    img && setProfileImage(img.fluid);
    setScholarProfile(profile ? Object.values(profile)[0] : null);
    setDetails(filteredDetailsArr);
  }, []);

  const html = data.markdownRemark.html;

  return (
    <Layout data={filteredNav}>
      <SEO title="" />
      <div className="xl:px-25 lg:px-20 sm:px-12 px-5 sm:py-20 py-10 mx-auto max-w-md">
        <h2 className="text-4xl font-bold text-blue-500">{name}</h2>
        <div
          className="mb-10 mt-5 xs:grid gap-x-1"
          style={{ gridTemplateColumns: profileImage && "30% auto" }}
        >
          <div className="">
            {profileImage && (
              <Image fluid={profileImage} className="md:mb-0 mb-5" />
            )}
            {/* {!profileImage && (
              <img src={noPhoto} alt="no photo" className="md:mb-0 mb-5" />
            )} */}
          </div>
          <div className={`${profileImage && "xs:ml-5"}`}>
            {interests && (
              <div className="mb-5 w-5/6">
                <h3 className="text-2xl mb-2 font-bold">Research interests</h3>
                <p className="">{interests}</p>
              </div>
            )}
            <div>
              <h3 className="text-2xl mb-2 font-bold">
                Contact and further information
              </h3>
              <ul className="">
                {details &&
                  details.map((el) => {
                    const key = Object.keys(el)[0];
                    const value = Object.values(el)[0];
                    return (
                      <li className="mb-1">
                        <span className="font-bold">
                          {key.charAt(0).toUpperCase() + key.slice(1)}:
                        </span>{" "}
                        {/* {value.includes("@") && <Mail mailAddress={value} />} */}
                        <CheckFormat data={value} />
                      </li>
                    );
                  })}

                {scholarProfile && (
                  <li className="mb-1">
                    <span className="font-bold cursor-pointer">
                      <a
                        href={scholarProfile}
                        rel="noreferrer"
                        target="_blank"
                        className=""
                      >
                        Link to Google Scholar Profile
                      </a>
                    </span>
                  </li>
                )}
              </ul>
            </div>
          </div>
        </div>
        <div>
          <article>
            <section
              className="markdown"
              dangerouslySetInnerHTML={{ __html: html }}
            />
          </article>
        </div>
      </div>
    </Layout>
  );
};

export default Bio;

const Tel = ({ number }) => <a href={`tel:${number}`}>{number}</a>;
const Mail = ({ mailAddress }) => (
  <a href={`mailto:${mailAddress}`}>{mailAddress}</a>
);
const Url = ({ url }) => (
  <a href={!url.includes("http") ? `https://${url}` : url}>{url}</a>
);

const CheckFormat = ({ data }) => {
  const regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/;
  const regTel = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
  const regUrl = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/;
  return (
    <>
      {regEmail.test(data) ? (
        <Mail mailAddress={data} />
      ) : regTel.test(data) ? (
        <Tel number={data} />
      ) : regUrl.test(data) ? (
        <Url url={data} />
      ) : (
        data
      )}
    </>
  );
};

export const pageQuery = graphql`
  query BioPage($name: String!) {
    site {
      siteMetadata {
        title
      }
    }
    images: allImageSharp {
      edges {
        node {
          parent {
            ... on File {
              name
            }
          }
          fluid {
            ...GatsbyImageSharpFluid_tracedSVG
          }
        }
      }
    }
    navigation: allContentTypesYaml {
      edges {
        node {
          header {
            identity {
              text
              image
              link
            }
            navigation {
              dropdown {
                link
                text
              }
              link
              text
            }
          }
        }
      }
    }
    markdownRemark(frontmatter: { name: { eq: $name } }) {
      id
      frontmatter {
        email
        image
        description
        name
        title
        website
        scholarProfile
        publications
        interests
      }
      html
    }
    navigation: allContentTypesYaml {
      edges {
        node {
          header {
            identity {
              text
              image
              link
            }
            navigation {
              dropdown {
                link
                text
              }
              link
              text
            }
          }
        }
      }
    }
  }
`;
